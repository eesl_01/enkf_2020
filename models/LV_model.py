"""
Lotka-Volterra Model. Taken from Matlab
code << lotkavolterra_2levels_sin_dataGEN_forEnKF.m >>
written by Olaf Cirpka and Daniel Erdal, University of Tubingen.
@author: Emilio Sanchez-Leon
"""
import sys, os, time, numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import interpolate
import scipy.sparse as ssp
import scipy.stats as sst
from scipy.integrate import odeint


def lotkavolterra_dataGEN_forEnKF(tOut, std_meas=None, plot=False, params=None, N0=None):
    """ Data generation for the Lotka-Volterra model with a sinusodial term. The code is a translation from
    Matlab code << lotkavolterra_2levels_sin_dataGEN_forEnKF.m >> written by Daniel Erdal, University of Tubingen.
    :parameters
        :param tOut: np. array, model output times in years
        :param std_meas: float or numpy array, measurement standard deviation for the population sizes. Optional. Default None
        :param plot: boolean, generates a plot with model results if True. Default False
        :param params: list, additional model parameters. [kgr1, kdec2, k12, Y21, tp]
        :param N0: list, initial population sizes for the prey and predator. Default None (1,1).
    :returns
        np.array, 2 x len(tOut) with the prey and predator populations at each tOut
    """
    # Lotka-Volterra-Model with 2 levels
    # N: Population size
    # N(1): Prey
    # N(2): Predator
    # dN1/dt = kgr1*N1 -k12*N1*N2
    # dN2/dt=  k12*Y21*N1*N2 -kdec3*N3
    # kgr1  : Growth rate of prey [1/a]
    # kdec3 : Death rate of predator [1/a]
    # k12   : Attac rate [1/a/pop.size]
    # Y21   : Predator offspring efficiency
    # tp    : Alpha factor controlling sine frequency. Temporal trend factor [1/a]

    # Parameter values
    if params:
        kgr1, kdec2, k12, Y21, tp = params
    else:
        kgr1=6  # 1/a
        kdec2=2  # 1/a
        k12=0.6  #1/a
        Y21 = 0.4
        tp=7

    # Output times:
    # tOut = np.arange(0.1, 20, 0.1)  # Output times for the model
    # tOut_ode = np.arange(0.1,20, 0.01)  # For the ode solver
    tOut_ode = np.linspace(tOut.min(), tOut.max(), num=len(tOut)*10)  # For the ode solver 10 times finer than tOut

    # Initial population sizes (prey, predator)
    if not N0:
        N0 = [6, 6]

    # Solve the system of equations:
    N_ode = odeint(lotkavolterraDGL_2L_sin, N0, tOut_ode,args=(kgr1, kdec2, k12, Y21, tp))

    # Interpolate to get thge solution only at tOut:
    fint_prey = interpolate.interp1d(tOut_ode, N_ode[:,0])
    fint_predator = interpolate.interp1d(tOut_ode, N_ode[:, 1])
    prey = fint_prey(tOut)
    predator = fint_predator(tOut)
    if std_meas:
        prey += np.random.rand(len(tOut))*std_meas
        predator += np.random.rand(len(tOut))*std_meas

    if plot is True:
        fig, ax = plt.subplots(3, 1)

        # todo: plot both together:
        ax[0].set_ylabel('population size', fontsize=16)
        ax[0].plot(tOut_ode, N_ode[:, 0], 'r-', label='prey model', alpha=.5)
        ax[0].plot(tOut, prey, 'r*', label='prey observations', alpha=.5)
        ax[0].plot(tOut_ode, N_ode[:, 1], 'b-', label=' predator model', alpha=.5)
        ax[0].plot(tOut, predator, 'b*', label='predator observations', alpha=.5)
        ax[0].set_xticklabels([])
        ax[0].tick_params(axis='y', labelsize=16, labelcolor='k')
        ax[0].legend(fontsize=10)

        ax[1].set_ylabel('no. of preys', fontsize=16)
        ax[1].plot(tOut_ode, N_ode[:,0], 'r-', label='model', alpha=.5)
        ax[1].plot(tOut, prey, 'r*', label='observations', alpha=.5)
        ax[1].set_xticklabels([])
        ax[1].tick_params(axis='y', labelsize=16, labelcolor='k')
        ax[1].legend(fontsize=16)

        ax[2].set_ylabel('no. of predators', fontsize=16)
        ax[2].set_xlabel('Time (years)', fontsize=16)
        ax[2].plot(tOut_ode, N_ode[:, 1], 'b-', label=' model', alpha=.5)
        ax[2].plot(tOut, predator, 'b*', label='observations', alpha=.5)
        ax[2].tick_params(axis='x', labelsize=16, labelcolor='k')
        ax[2].tick_params(axis='y', labelsize=16, labelcolor='k')
        ax[2].legend(fontsize=16)
        #fig.tight_layout()
        #ax[0].text(60, .025, r'$\mu=100,\ \sigma=15$')
    # todo: add a banner to print the parameter values used for data generation.

    return np.c_[prey, predator]

def lotkavolterra(kgr1, kdec2, k12, Y21, tp, N0, tOut):
    """" State-space Lotka-Volterra model with a sinusodial term. The code is based on the
    Matlab code << lotkavolterra_2levels_sin_dataGEN_forEnKF.m >> written by Daniel Erdal, University of Tubingen.
    :parameters
        :param kgr1:
        :param kdec2:
        :param k12:
        :param Y21:
        :param tp:
        :param N0:
        :param tOut:
        :param tOutRet:
    :returns
        np.array, 2 x len(tOut) with the prey and predator populations at each tOut
    """
    # Lotka-Volterra-Model with 2 levels
    # N: Population size
    # N(1): Prey
    # N(2): Predator
    # dN1/dt = kgr1*N1 -k12*N1*N2
    # dN2/dt=  k12*Y21*N1*N2 -kdec3*N3
    # kgr1  : Growth rate of prey [1/a]
    # kdec3 : Death rate of predator [1/a]
    # k12   : Attac rate [1/a/pop.size]
    # Y21   : Predator offspring efficiency
    # tp    : Alpha factor controlling sine frequency. Temporal trend factor [1/a]

    # Parameter values

    # Output times:
    # tOut = np.arange(0.1, 20, 0.1)  # Output times for the model
    # tOut_ode = np.arange(0.1,20, 0.01)  # For the ode solver
    # dt = 0.01

    # if len(tOut) < 2:
    #     tOut_ode = np.arange(0, tOut+dt, dt)  # For the ode solver 10 times finer than tOut
    # else:
    tOut_ode = np.linspace(tOut.min(), tOut.max(), num=len(tOut)*100)
    # Solve the system of equations:
    # kdec2 = 2
    N_ode = odeint(lotkavolterraDGL_2L_sin, N0, tOut_ode,args=(kgr1, kdec2, k12, Y21, tp))

    # Interpolate to get the solution only at tOut:
    fint_prey = interpolate.interp1d(tOut_ode, N_ode[:,0])
    fint_predator = interpolate.interp1d(tOut_ode, N_ode[:, 1])

    prey = fint_prey(tOut)
    predator = fint_predator(tOut)

    # return np.c_[prey, predator].flatten('F')
    return np.c_[prey, predator]

def read_obs_lv(myfile, cols2load=None):

    print('')

def lotkavolterraDGL_2L_sin(N,t,kgr1, kdec2, k12, Y21, tp):
    """ Lotka-Volterra-Model with 2 trophischen steps, as state-space formulation.
        dN1/dt = kgr1*N0 -k12*N0*N1
        dN2/dt = k12*Y21*N0*N1 -kdec2*N1
    Parameters:
        :param N     : Population size, N[0]: Prey size, N[1]): Predator size
        :param kgr1  : Growth rate of prey [1/a]
        :param kdec2 : Death rate of predator [1/a]
        :param k12   : Attack rate [1/a/pop.size]
        :param Y21   : Predator offspring efficiency
        :param tp    : Alpha factor controlling sine frequency. Temporal trend factor [1/a]
    Return:
        system of odes computed, as a np.array([dNdt1,dNdt2])
    """
    dNdt1=kgr1*N[0]*((np.sin(2*np.pi*t*tp/10))/4+1)-k12*N[0]*N[1]
    dNdt2=k12*N[0]*N[1]*Y21-kdec2*N[1]
    return np.array([dNdt1,dNdt2])


# Create a Synthetic dataset and store things:
create_snthetic_set = False
# todo: document the variables in here
if create_snthetic_set:
    t = np.arange(0.1,20.1,0.1)
    kgr1=6  #1/a, to use for assimilation
    kdec2=2  # 1/a, to use for assimilation
    k12=0.6  #1/a
    Y21 = 0.4
    tp=7
    N0 = [6, 6]
    Nmod = lotkavolterra_dataGEN_forEnKF(t, params=[kgr1, kdec2, k12, Y21, tp], N0=N0, std_meas=0.02, plot=True)

    # Which time steps will be accounted as observations:
    tbool = np.zeros(len(t))
    tbool[14:100:1] = 1
    saveDir = r'D:\Users\Emilio\_codedev\modified_enkf\_da\lotkavol_da\model_data'
    # save time outputs:
    np.savetxt(os.path.join(saveDir, '_output_times.txt'), np.transpose((t, tbool)), fmt='%.3f %d', header='time in years')
    np.savetxt(os.path.join(saveDir, 'prey_predator_data.txt'), Nmod, fmt='%.3f', header=' no. prey, no. predator')

    print('Pause')