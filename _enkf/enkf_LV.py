"""
Data assimilation with the Lotka-Volterra model
States (population sizes) = prey, predators
Parameters:
    kgr1 Growth rate of prey [1/a], 2update
    tp   Temporal trend factor [1/a], 2update
    k12  Attack rate [1/a/pop.size]
    Y21   Predator offspring efficiency
    kdec2 Death rate of predator [1/a]
Created on Monday 08.06.2020
@author: Emilio Sanchez-Leon
"""
import os
import scipy
import scipy.sparse as ssp
import sys
import time
import multiprocessing as mp
import numpy as np, matplotlib.pylab as plt
from scipy import stats

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'enkf_utilities')))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),'..', 'models')))
import LV_model as LV
import enkf_utilities.hgs as hgr
import enkf_utilities.forwardmodel as fw
import enkf_utilities.gridmanip as gm
import enkf_utilities.dirs as dm
import enkf_utilities.fieldgen as fg
import enkf_utilities.matmult.matmul_opt as mm
import enkf_utilities.matmult.mylikelihood as mml
import enkf_utilities.kalman as kf
import enkf_utilities.mystats as mysst
import enkf_utilities.data as dc
import enkf_utilities.plotResults as pptr

# def enkf(modelname, **mysettings):
# todo: here I load this to actually be able to run something
model_name = 'LV'
test_name = 'LV_700mem_prey'
homedir = r'D:\Users\Emilio\_codedev\modified_enkf'
data_file = 'prey_predator_data.txt'
times_file = '_output_times.txt'
nmem = 200
#upd_params = True
nupd_param = 2
#upd_states = True
nupd_states = 2
da_method = 'enkf'
OL = False
parallel = True
cpus = 4
perturb_data = True
obs_noise = np.sqrt(0.03)
prey_obs = True
pred_obs = True

da_dirs = dm.init_maindir(homedir, modelname=model_name, testname=test_name)
# Get full reference data:
ref_data = np.loadtxt(os.path.join(da_dirs['model_data_dir'], data_file))
ref_times, upd_bool = np.loadtxt(os.path.join(da_dirs['model_data_dir'], times_file), unpack=True, dtype='float,bool')

# Get observations that will be used for update:
data2upd = ref_data[upd_bool,:]             # Model specific
if data2upd.ndim == 1:
    data2upd = data2upd[:, np.newaxis]
time2upd = ref_times[upd_bool]
data_loc = None # TODO: not sure about this

# Time step controller:
run_values, run_starts, run_lengths, true_values = kf.find_runs(upd_bool)
nupd_stp = np.sum(run_lengths[run_values])
# actual time steps: for HGS after every update, time steps reset to zero. Not for LV. Not really for PArflow.


# Initialize state/parameter matrices:
ndimX = (nupd_param + nupd_states, nmem)
priorX = np.ones(ndimX)  # Parameter and states
postX = np.empty(ndimX)

# Perturbing all measurements:
if perturb_data:
    flat_data = data2upd.flatten('F')
    meas_std = np.ones(flat_data.shape) * obs_noise
    refDataObj = dc.DataClass(flat_data, nmem)
    cdd_all = refDataObj.get_cdd(meas_std)  # This is R in my original code
    pert_all = refDataObj.pert_chol(cdd_all).reshape(data2upd.shape + (nmem,), order='F').squeeze()
else:
    pert_all = np.zeros(data2upd.shape + (nmem,)).squeeze()
    # meas_std = np.ones((len(data2upd))) * obs_noise
    # refDataObj = dc.DataClass(data2upd, nmem)
    # cdd_all = refDataObj.get_cdd(meas_std)  # This is R in my original code
    # pert_all = refDataObj.pert_chol(cdd_all)
    # data_all_pert = data2upd + pert_all
np.save(os.path.join(da_dirs['model_data_dir'], 'data_perturbations_%s' % test_name), pert_all)


# Define the update steps, according to the type of update: EnKF, KEG, Restart, Smoother.
# If standard EnKF:
# if da_method is 'enkf':
#     time_steps = np.r_[upd_time[0], np.diff(upd_time)]

# Define model parameters and initial model states: Model specific
# kgr1 = 6  # Growth rate of prey [1/a], 2update
# tp = 2  # Temporal trend factor [1/a], 2update

k12 = 0.6  # Attack rate [1/a/pop.size]
Y21 = 0.4   # Predator offspring efficiency
kdec2 = 2   # Death rate of predator [1/a]

N0 = np.ones((2, nmem))
N0[0, :] *= 6
N0[1, :] *= 6
# initial_states = (N0[0, :], N0[1, :])
pbL, pbU = 1, 10  # For LV, use random generator with lower and upper bounds
priorX[0:2, :] = (pbL + (pbU - pbL) * np.random.rand(2, nmem))  # [3, 3]

if nupd_states:
    priorX[nupd_param:, :] = N0
np.save(os.path.join(da_dirs['param_dir'], 'priorX.npy'), priorX)
# Model specific: end

plt.plot(ref_times, ref_data[:,0])
plt.plot(time2upd, data2upd, 'bo')
if OL:  # Run open loop if True:
    for cur_mem in range(0, nmem):
        # Model specific: start
        upd_parameters = (priorX[0, cur_mem], priorX[1, cur_mem])
        model_params = (k12, Y21, kdec2, N0[:, cur_mem], ref_times)
        # Model specific:end

        fwObject = fw.ForwardModel(cur_mem, da_dirs['states_dir'], None, None, upd_parameters, *model_params, mode='OL')

        # Model specific:start
        fwObject.fm_LV_model()
        # TODO: read model observations and store them in a specific directory

        # Model specific:end
    OL_files = dm.getdirs(da_dirs['states_dir'], mystr='out.OL', fullpath=True)
    OL_obs = np.empty((len(ref_times), ref_data.ndim, nmem))
    for ii, cur_file in enumerate(OL_files):
        OL_obs[:, :, ii] = np.loadtxt(cur_file)
    np.save(os.path.join(da_dirs['obs_dir'], 'LV_all.obs.OL.npy'), OL_obs)


# Define necessary counters:
counter_updSteps = 0
counter_simsteps = 0

for cur_run_value in range(0, len(run_values)):
    brk_while = 0  # counter for the while loop
    doSomething_flag = True  # Flag to run models and updates
    while brk_while < run_lengths[cur_run_value]:

        if (run_values[cur_run_value] == True) and (run_lengths[cur_run_value] > 1):  # If consecutive update steps
            #counter_updSteps += 1
            counter_simsteps += 1
            tOut = ref_times[run_starts[cur_run_value] + brk_while:run_starts[cur_run_value] + brk_while + 2]
            brk_while += 1
            if brk_while == run_lengths[cur_run_value]:
                break
        elif (run_values[cur_run_value] == True) and (run_lengths[cur_run_value] == 1):  # If only one update step
            #counter_updSteps += 1
            brk_while += 1
            counter_simsteps += 1
            doSomething_flag = False        # If the one-update step is at the end, it has been updated already
            if cur_run_value == 0:      # If the one-update step is at the beginning of all DA
                brk_while += -1
                doSomething_flag = True
                tOut = np.array([0, ref_times[0]])

        elif not run_values[cur_run_value]:  # If model will simulate several steps without update
            brk_while = run_lengths[cur_run_value] + 1
            # counter_updSteps += 1
            counter_simsteps = np.sum(run_lengths[0:(cur_run_value + 1)])
            tOut = ref_times[run_starts[cur_run_value]:run_starts[cur_run_value] + run_lengths[cur_run_value] + 1]

        # Prepare things for update
        if doSomething_flag is True:

            if (counter_updSteps < len(data2upd)) or (run_values[cur_run_value] == True):
                fix_counter_updStates = counter_updSteps #-1
                sel_data = np.asarray(data2upd[counter_updSteps]) # ref_data[counter_simsteps, np.newaxis]

                # How to select
                if pert_all.ndim == 2:
                    sel_pert = pert_all[np.newaxis, counter_updSteps, :]
                elif pert_all.ndim == 3:
                    sel_pert = pert_all[counter_updSteps, :, :]
                # Get cdd (or R) for the current data:
                meas_std = np.ones((len(sel_data))) * obs_noise
                refDataObj = dc.DataClass(sel_data, nmem)
                cdd = refDataObj.get_cdd(meas_std)

                # Initialize required variables:
                res_o = np.empty((len(sel_data), nmem))
                llkhd_o = np.empty((nmem))
                mod_data = np.empty((len(sel_data), nmem))
                mod_data_new = np.empty((len(sel_data), nmem))
                res_new = np.empty((len(sel_data), nmem))
                llkhd_new = np.empty((nmem))

            for cur_mem in range(0, nmem):
                upd_parameters = (priorX[0, cur_mem], priorX[1, cur_mem])
                #initial_states = (N0[0, cur_mem], N0[1, cur_mem])
                model_params = (k12, Y21, kdec2, N0[:,cur_mem], tOut)
                fwObject = fw.ForwardModel(cur_mem, da_dirs['states_dir'], run_values[cur_run_value], ref_times,
                                           upd_parameters, *model_params, mode='assim')
                cur_states = fwObject.fm_LV_model()

                # Get current model states, if upd_states is True:
                if nupd_states:
                    postX[nupd_param:, cur_mem] = cur_states
                # Get current model parameters:
                postX[0:nupd_param, cur_mem] = priorX[0:nupd_param, cur_mem]
                # Get model output (observations):
                if prey_obs and pred_obs:
                    obs_temp= cur_states
                elif prey_obs:
                    obs_temp = cur_states[0]
                elif pred_obs:
                    obs_temp = cur_states[1]

                mod_data[:, cur_mem] = obs_temp

                if counter_simsteps < len(ref_data) or (run_values[cur_run_value] == True):
                    res_o[:, cur_mem], llkhd_o[cur_mem] = refDataObj.likelihood(sel_data + sel_pert[:, cur_mem], mod_data[:, cur_mem], cdd)

            np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.obs.%.3d.txt' % (counter_simsteps)), mod_data, header="observations. t=%5.4e" % tOut[-1])
            np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.res.%.3d.txt' % (counter_simsteps)), res_o,header="residuals (data-simdata). t=%5.4e" % tOut[-1])
            np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.lkl.%.3d.txt' % (counter_simsteps)), llkhd_o, header="likelihood. t=%5.4e" % tOut[-1])

            print('Stop pushing ensemble forward. Stopped at simulation time step %s (t=%s)' % (counter_simsteps, tOut[-1]))

            if counter_simsteps < len(ref_data) or (run_values[cur_run_value] == True):
                pptr.plotlikelihood(llkhd_o, res_o, cdd, fignum='%sa' % (counter_updSteps+1),
                                    savefile=os.path.join(da_dirs['plot_dir'], 'LV_likelihood.%.3d' % counter_simsteps))

                print('Initializing update step %s (total steps %s)...' % ((counter_updSteps+1), np.sum(upd_bool)))
                
                updObj = kf.UpdateClass(postX)
                cyy = updObj.get_cyy(mod_data)
                cxy = updObj.get_cxy(mod_data)
                kgain = updObj.kgain(cxy, cyy, cdd, beta=1, alpha=1)
                priorX, d_minus_y1, upd1 = updObj.upd_eq(kgain, sel_data, mod_data, sel_pert, damp=0.9)

                #plt.plot(np.ones((nmem,)) * tOut[-1], mod_data[0, :], 'mo')
                #plt.plot(np.ones((200,)) * tOut[-1], priorX[2, :], 'g.')
                # If upd_states is False, re-run entire ensemble to get updated states
                if not nupd_states:
                    print('Updating model states by re-running ensemble with updated parameters (update step %s, from %s)' % ((counter_updSteps+1), np.sum(upd_bool)))
                    # Re-define initial states: (model dependent?)
                    tOut_updated = np.array([tOut[0], tOut[-1]])
                    for cur_mem in range(0, nmem):
                        upd_parameters = (priorX[0, cur_mem], priorX[1, cur_mem])
                        model_params_upd = (k12, Y21, kdec2, N0[:,cur_mem], tOut_updated)
                        fwObject = fw.ForwardModel(cur_mem, da_dirs['states_dir'], run_values[cur_run_value], ref_times,
                                                   upd_parameters, *model_params_upd, mode='upd')
                        cur_states_upd = fwObject.fm_LV_model()

                        st_string = 'LV_%.3d.out.%.3d.upd.txt' % (cur_mem, counter_simsteps)
                        np.savetxt(os.path.join(da_dirs['states_dir'], st_string), cur_states_upd.T, header="prey,predator. t=%5.4e" % tOut[-1])

                        # Get updated model output:
                        if prey_obs and pred_obs:
                            obs_temp = cur_states_upd
                        elif prey_obs:
                            obs_temp = cur_states_upd[0]
                        elif pred_obs:
                            obs_temp = cur_states_upd[1]
                        mod_data_new[:, cur_mem] = obs_temp


                        # kgr1, kdec2, k12, Y21, tp, N0, tOut
                        res_new[:, cur_mem], llkhd_new[cur_mem] = refDataObj.likelihood(sel_data + sel_pert[:, cur_mem],
                                                                                        mod_data_new[:, cur_mem], cdd)
                        N0[:, cur_mem] = cur_states_upd

                    # np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.obs.%.3d.out.txt' % (counter_simsteps)), mod_data_new,
                    #            header="observations. t=%5.4e" % tOut[-1])
                    # np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.res.%.3d.out.txt' % (counter_simsteps)), res_new,
                    #            header="residuals (data-simdata). t=%5.4e" % tOut[-1])
                    # np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.lkl.%.3d.out.txt' % (counter_simsteps)), llkhd_new,
                    #            header="likelihood. t=%5.4e" % tOut[-1])

                    # pptr.plotlikelihood(llkhd_new, res_new, cdd, fignum='%sb' % (counter_updSteps),
                    #                     savefile=os.path.join(da_dirs['plot_dir'], 'LV_likelihood.%.3d.upd' % counter_simsteps))

                    # Assign the updated states to the initial states for next time step: Model dependenet

                elif nupd_states:

                    for cur_mem in range(0, nmem):
                        # Get updated model output:
                        if prey_obs and pred_obs:
                            obs_temp = priorX[2:, cur_mem]
                        elif prey_obs:
                            obs_temp = priorX[2, cur_mem]
                        elif pred_obs:
                            obs_temp = priorX[3, cur_mem]
                        mod_data_new[:, cur_mem] = obs_temp
                        res_new[:, cur_mem], llkhd_new[cur_mem] = refDataObj.likelihood(sel_data + sel_pert[:, cur_mem],
                                                                                    mod_data_new[:, cur_mem], cdd)
                        st_string = 'LV_%.3d.out.%.3d.upd.txt' % (cur_mem, counter_simsteps)
                        np.savetxt(os.path.join(da_dirs['states_dir'], st_string), priorX[nupd_param:, cur_mem].T,
                                   header="prey,predator. t=%5.4e" % tOut[-1])

                    # pptr.plotlikelihood(llkhd_new, res_new, cdd, fignum='%sb' % (counter_updSteps),
                    #                     savefile=os.path.join(da_dirs['plot_dir'], 'LV_likelihood.%.3d.upd' % counter_simsteps))
                    N0 = priorX[nupd_param:, :]      # This is because observation equal the states

                np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.obs.%.3d.upd.txt' % (counter_simsteps)), mod_data_new,
                           header="observations. t=%5.4e" % tOut[-1])
                np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.res.%.3d.upd.txt' % (counter_simsteps)), res_new,
                           header="residuals (data-simdata). t=%5.4e" % tOut[-1])
                np.savetxt(os.path.join(da_dirs['obs_dir'], 'LV_all.lkl.%.3d.upd.txt' % (counter_simsteps)), llkhd_new,
                           header="likelihood. t=%5.4e" % tOut[-1])

                pptr.plotlikelihood(llkhd_new, res_new, cdd, fignum='%sb' % (counter_updSteps),
                                    savefile=os.path.join(da_dirs['plot_dir'], 'LV_likelihood.%.3d.upd' % counter_simsteps))
                accepted = refDataObj.acc_rej_chi2(llkhd_o, refDataObj.ndata)

                plt.plot(np.ones((nmem,)) * tOut[-1], mod_data_new[0, :], 'g.')

            counter_updSteps += 1
            print('Cumulative time steps: %s ' % counter_simsteps)

            # Reorganize results and prepare for plotting

assert nupd_stp == (counter_updSteps-1), 'Oops something is messed up with the time stepping...'

print('DA finished')