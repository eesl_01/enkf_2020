""" Functions available to perform Kalman Filter inversion """
import os
import time
import numpy as np
import datetime
import shutil
import sys
import scipy.sparse as ssp
import scipy.stats as sst
import platform
import multiprocessing as mp
from itertools import repeat
import hgs as hgr
from numpy.linalg import inv, solve
import dirs as dm
import gridmanip as gm
import matmult.mylikelihood as mml
import matmult.matmul_opt as mm
import mystats as msst


#   0- res_likelihood(res_likelihood(field_data, modmeas_pert, R, parmatmult, mymethod='solve')
#   1- acc_rej(Likelihood_new, Likelihood_old,chi2, acceptedReal, acc_flag)
#   2- sequential_update(ii, R, Kgain, Y, nreal, nmeas, fieldmeas, modmeas, E, Y_old, acceptedReal, dY_max, dY_min, heads, concentrations)
#   3- readLastIterData(Inv_name, myType = '',my_timest = '')
#   4- checkLastIterFiles(Inv_name,my_timest = '')
#   5- expshr_param(ii, mode, Y, kkkFile, orig_indices, sel_indices, meanVal = '', what= '')
# -------------------------------------------------------------------------------
class UpdateClass(object):
    """
    Data class. Contains methods related to the data used for assimilation
    """
    def __init__(self, X):
        self.X = X
        self.nreal = X.shape[1]
        self.nX = X.shape[0]

    def get_cyy(self, modout):
        """ Compute the covariance matrix of the model predictionsCross cov """

        cyy = mml.autocov(modout, diag=False)

        return cyy

    def get_cxy(self, modout):
        """ Compute cross covariance between model parameters and model output """

        cxy = mml.crosscov(self.X, modout, diag=False)

        return  cxy

    def kgain(self, cxy, cyy, cdd, beta=1, alpha=1):
        """ Kalman Gain """
        # %% Compute the Kalman gain:
        # K = Qym inv( Qmm + R ) (nel x nmeas)
        # ------------------------------------------------------------------------------------------------
        # First method, using the inverse
        Kgain = (cxy.dot(np.linalg.inv(cyy + beta*alpha*cdd)))
        # Second method using the backslash operator. This is numerically more stable.
        #Kgain = np.linalg.solve((cyy + beta * alpha * cdd), cxy.transpose()).transpose()

        return Kgain

    def upd_eq(self, Kgain, data, modout, perturbation, damp=1):
        """ Update equation using Kalman gain explicitly
        :param Kgain:
        :param data:
        :param modout:
        :param perturbation:
        :param damp:
        :return:
        """
        # Compute deviation of perturbed data and model output
        # d_minus_y = data(:)*ones(1,sum(select)) + dpert(:,select) - Y(:,select); FROM OLAF
        d_minus_y = data[:, np.newaxis]+ perturbation - modout
        interm = np.dot(Kgain, d_minus_y)
        Xpost = self.X + damp * interm
        Xpost = np.squeeze(np.asarray(Xpost))
        return Xpost, d_minus_y, interm

    def upd_eq_noKgain(self, cyy, cdd, data, modout, perturbation, damp=1, beta=1, alpha=1):
        """ Kalman update step without explicitly calculating the Kalman gain
        % Kalman update step
                  Xpost(:,select) = Xpost(:,select) + info.damp.*Xprime*(Yprime'*((Cyy+beta*alpha*Cdd)\d_minus_y));
        """
        # todo: check dimensions here
        Xprime = self.X - np.dot( np.nanmean(self.X, axis=-1)[:, np.newaxis], np.ones((1, self.nreal)))
        Yprime = modout - np.dot( np.nanmean(modout, axis=-1)[:, np.newaxis], np.ones((1, self.nreal)))

        dev_mean = modout - np.average(modout, axis=1, weights=None, returned=False)[:, np.newaxis]

        d_minus_y = data[:, np.newaxis] + perturbation - modout

        Xpost = self.X + damp * np.dot(Xprime,np.dot(np.transpose(Yprime),np.linalg.solve(cyy + beta*alpha*cdd, d_minus_y)))

        return Xpost, d_minus_y, damp * np.dot(np.dot(Xprime,np.transpose(Yprime)),np.linalg.solve(cyy + beta*alpha*cdd, d_minus_y))

def find_runs(x):
    """Find runs of consecutive items in an array. Source: alimanfoo/find_runs.py (by 02.06.2020).
       Github repository belongs to Alistair Miles (alimanfoo) """

    # ensure array
    x = np.asanyarray(x)
    if x.ndim != 1:
        raise ValueError('only 1D array supported')
    n = x.shape[0]

    # handle empty array
    if n == 0:
        return np.array([]), np.array([]), np.array([])

    else:
        # find run starts
        loc_run_start = np.empty(n, dtype=bool)
        loc_run_start[0] = True
        np.not_equal(x[:-1], x[1:], out=loc_run_start[1:])
        run_starts = np.nonzero(loc_run_start)[0]

        # find run values
        run_values = x[loc_run_start]

        # find run lengths
        run_lengths = np.diff(np.append(run_starts, n))

        # find True locations:
        upd_values = np.where(run_values == True)

        return run_values, run_starts, run_lengths, upd_values[0]


def caller_kfupdate(homedir, str_assim, nnodes, mymode, type_update, dY_max, dY_min, modmeas_pert, Y, current_time,
                    thetimes, nobs, selfieldmeas, R, Kgain, parallel=False, cpus='', parmatmult=False, initial_head=7.0,
                    neg2zero=False, head2dwdn=False, cumbtc=False, norm=False, moments=False, OWnode='',
                    porosity=False, storativity=False, dispersivity=False, extraparm_idx=[], cluster=False,
                    subgrid=False, subgridnodes=None, subgridelem=None, hgs='2016'):
    """

    Args:
        homedir:
        str_assim:
        nnodes:
        mymode:
        type_update:
        dY_max:
        dY_min:
        modmeas_pert:
        Y:
        current_time:
        thetimes:
        nobs:
        selfieldmeas:
        R:
        Kgain:
        parallel:
        cpus:
        parmatmult:
        initial_head:
        neg2zero:
        head2dwdn:
        cumbtc:
        norm:
        moments:
        OWnode:
        porosity:
        storativity:
        dispersivity:
        extraparm_idx:
        cluster:

    Returns:

    """
    nel, nreal = Y.shape
    if (type_update == 'cum') and (current_time < len(thetimes)):
        modmeas_upd = np.zeros((modmeas_pert.shape[0] + nobs, modmeas_pert.shape[1]))
        residuals = np.zeros((modmeas_pert.shape[0] + nobs, modmeas_pert.shape[1]))
    else:
        modmeas_upd = np.zeros(modmeas_pert.shape)
        residuals = np.zeros(modmeas_pert.shape)
    likelihood = np.zeros((1, nreal))
    Y_new = np.zeros(Y.shape)

    if cluster is True:
        helperPath = os.path.join(homedir, 'realizations_%s' % str_assim, 'tempY')
        if not os.path.exists(helperPath):
            os.makedirs(helperPath)
        for aa in range(0, nreal):
            np.save(os.path.join(helperPath, 'Y_%.3d' % aa), Y[:, aa])
        Y = helperPath

        # Do the same with the kalman gain
        helperPathKalman = os.path.join(homedir, 'realizations_%s' % str_assim, 'tempKgain')
        if not os.path.exists(helperPathKalman):
            os.makedirs(helperPathKalman)
        np.save(os.path.join(helperPathKalman, 'Kgain_iter_%.3d' % current_time), Kgain)
        Kgain = helperPathKalman

    # if mymode == 'tr_':
    #     headfield_tt = np.empty((1, nreal), dtype='|S')
    # else:
    #     headfield_tt = np.zeros((nnodes, nreal))
    if parallel is False:
        for zz in range(0, nreal):
            Y_new[:, zz], modmeas_upd[:, zz], residuals[:, zz], likelihood[0, zz], zz_idx = \
                kfupdate(zz, homedir, str_assim, nnodes, thetimes, selfieldmeas, modmeas_pert, R,
                         Kgain, Y, dY_max, dY_min, mymode, current_time,
                         parmatmult, type_update,
                         initial_head, neg2zero, head2dwdn, cumbtc, norm,
                         moments, OWnode, porosity,
                         storativity, dispersivity, extraparm_idx, subgrid, subgridnodes, subgridelem, hgs)

            assert zz_idx == zz, 'Oh oh, it seems there is a mess in the tracking system of realizations!!!'

    elif parallel is True:
        mypool = mp.Pool(cpus)
        full_results = mypool.starmap(kfupdate,
                                      zip(np.arange(0, nreal, 1), repeat(homedir), repeat(str_assim), repeat(nnodes),
                                          repeat(thetimes),
                                          repeat(selfieldmeas), repeat(modmeas_pert), repeat(R), repeat(Kgain),
                                          repeat(Y),
                                          repeat(dY_max), repeat(dY_min), repeat(mymode), repeat(current_time),
                                          repeat(parmatmult), repeat(type_update), repeat(initial_head),
                                          repeat(neg2zero), repeat(head2dwdn), repeat(cumbtc), repeat(norm),
                                          repeat(moments), repeat(OWnode), repeat(porosity), repeat(storativity),
                                          repeat(dispersivity), repeat(extraparm_idx), repeat(subgrid),
                                          repeat(subgridnodes), repeat(subgridelem), repeat(hgs)))
        mypool.close()
        mypool.join()
        # Here I have to manipulate the list to get the proper numpy arrays

        for xx in range(0, len(full_results)):
            Y_new[:, xx] = np.asarray(full_results[xx][0])
            modmeas_upd[:, xx] = np.asarray(full_results[xx][1])
            # headfield_tt[:, xx] = np.asarray(full_results[xx][2])
            residuals[:, xx] = np.asarray(full_results[xx][3])
            likelihood[:, xx] = np.asarray(full_results[xx][4])
            assert xx == int(
                full_results[xx][-1]), 'Oh oh, it seems there is a mess in the id tracking system of realizations!!!'

    return Y_new, modmeas_upd, residuals, likelihood


def kfupdate(tt, homedir, str_assim, nnodes, thetimes, selfieldmeas, modmeas_pert, R, Kgain, Y, dY_max, dY_min, mymode,
             current_time, parmatmult, type_update, initial_head, neg2zero, head2dwdn, cumbtc, norm, moments, OWnode,
             porosity, storativity, dispersivity, extraparm_idx, subgrid, subgridnodes, subgridelem, hgs):
    """

    Args:
        tt:
        homedir:
        str_assim:
        nnodes:
        thetimes:
        selfieldmeas:
        modmeas_pert:
        R:
        Kgain:
        Y:
        dY_max:
        dY_min:
        mymode:
        current_time:
        parmatmult:
        type_update:
        initial_head:
        neg2zero:
        head2dwdn:
        cumbtc:
        norm:
        moments:
        OWnode:
        porosity:
        storativity:
        dispersivity:
        extraparm_idx:
        subgrid:

    Returns:

    """
    if current_time < len(thetimes):
        hgr.prepfwd(tt, homedir, mymode, current_time, thetimes, headsFile='%ssim_%.5do.hen_prev' % (mymode, tt + 1),
                    str_assim=str_assim, type_update=type_update, concFile='%ssim_%.5do.cen_prev' % (mymode, tt + 1))
    # if not isinstance(initial_head, (int, float, str)):
    #     if initial_head:
    #         initial_head = initial_head[:, tt]

    Y_tt, modmeas_upd_tt, res, llh = update_step(tt, homedir, str_assim, nnodes, thetimes, selfieldmeas,
                                                               modmeas_pert[:, tt], R, Kgain, Y, dY_max, dY_min,
                                                               mymode=mymode, curtimestep=current_time,
                                                               parmatmult=parmatmult,
                                                               mytype=type_update, ini_head=initial_head,
                                                               neg2zero=neg2zero,
                                                               head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm,
                                                               moments=moments,
                                                               OWnode=OWnode, porosity=porosity,
                                                               storativity=storativity,
                                                               dispersivity=dispersivity, extraparm_idx=extraparm_idx,
                                                               subgrid=subgrid, subgridnodes=subgridnodes,
                                                               subgridelem=subgridelem, hgs=hgs)

    # if mymode != 'tr_':
    #     headfield_tt = headfield_tt.squeeze()
    return Y_tt.squeeze(), modmeas_upd_tt, res.squeeze(), llh, tt


def update_step(ii, homedir, str_assim, nnodes, modeltimes, fieldmeas, modmeas_pert, R, Kgain, Y, dY_max, dY_min,
                mymode='fl_', curtimestep=0, parmatmult=False, mytype='restart', ini_head=7, neg2zero=False,
                head2dwdn=False, cumbtc=False, norm=False, moments=False, OWnode='', porosity=False,
                storativity=False, dispersivity=False, extraparm_idx=[], subgrid=False, subgridnodes=None,
                subgridelem=None, hgs='2016'):
    """

    Args:
        ii:
        homedir:
        str_assim:
        nnodes:
        modeltimes:
        fieldmeas:
        modmeas_pert:
        R:
        Kgain:
        Y:
        dY_max:
        dY_min:
        mymode:
        curtimestep:
        parmatmult:
        mytype:
        ini_head:
        neg2zero:
        head2dwdn:
        cumbtc:
        norm:
        moments:
        OWnode:
        porosity:
        storativity:
        dispersivity:
        extraparm_idx:
        subgrid
    Returns:

    """
    # %% Directories and necessary data from the grid:
    # ---------------#
    NoReal, dir_dict, fmt_string, kfile = dm.init_dir(ii, mymode, homedir, str_assim=str_assim, porosity=porosity,
                                                      storativity=storativity, dispersivity=dispersivity)
    # kkkFile = os.path.join(dir_dict['fld_mode'], kfile)

    if type(Y) == str:  # this is only in case the cluster has some issues passing large arrays
        helperPath = Y
        # Y = np.empty((Kgain.shape[0], len(os.listdir(helperPath))))
        Y = np.load(os.path.join(helperPath, 'Y_%.3d.npy' % int(int(NoReal) - 1)))
        os.remove(os.path.join(helperPath, 'Y_%.3d.npy' % int(int(NoReal) - 1)))
        # for aa in range(0, NoReal):
        #     Y[:, aa] = np.load(os.path.join(helperPath, 'Y_%s.npy' % aa))
    else:
        helperPath = None

    if type(Kgain) == str:
        Kgain = np.load(os.path.join(Kgain, 'Kgain_iter_%.3d.npy' % curtimestep))
    # %% Start the process:
    # ---------------#
    restart_flag = True

    while restart_flag is True:

        # Update equation:
        Y_i = update_eq(Y, fieldmeas, modmeas_pert, R, parmatmult, Kgain, dY_max)

        if (porosity or storativity or dispersivity) is True:
            # time.sleep(np.random.uniform(low=1.0, high=3, size=1))  # Process_created = mp.Process()
            extra_param = Y_i[-extraparm_idx[0]:]
            Y_i = Y_i[0:-(extraparm_idx[0])]

            if (porosity is True) and (extraparm_idx[1] > 0):
                if (extraparm_idx[2] == 0) and (extraparm_idx[3] == 0):
                    porosities = extra_param[-extraparm_idx[0]:]
                else:
                    porosities = extra_param[-extraparm_idx[0]:(-extraparm_idx[0] + 3)]
                hgr.update_mpropsFile('porosity', np.exp(porosities),
                                      os.path.join(dir_dict['fld_mode'], 'lauswiesen.mprops'))
                porosityFile = os.path.join(dir_dict['fld_porosity'], 'porositytemp%s' % ii)
                np.save(porosityFile, np.exp(porosities[:]))

            if (storativity is True) and (extraparm_idx[2] > 0):
                if extraparm_idx[3] == 0:
                    storativities = extra_param[-(extraparm_idx[-1] + extraparm_idx[-2]):]
                else:
                    storativities = extra_param[-(extraparm_idx[-1] + extraparm_idx[-2]):(-extraparm_idx[-1])]
                hgr.update_mpropsFile('storage', np.exp(storativities),
                                      os.path.join(dir_dict['fld_mode'], 'lauswiesen.mprops'))
                storageFile = os.path.join(dir_dict['fld_storativity'], 'storativitytemp%s' % ii)
                np.save(storageFile, np.exp(storativities[0]))

            if (dispersivity is True) and (extraparm_idx[3] > 0):
                dispersivities = extra_param[-extraparm_idx[-1]]
                hgr.update_mpropsFile('dispersivity', np.exp(dispersivities),
                                      os.path.join(dir_dict['fld_mode'], 'lauswiesen.mprops'))
                dispFile = os.path.join(dir_dict['fld_dispersivity'], 'dispersivitytemp%s' % ii)
                np.save(dispFile, np.exp(dispersivities[0]))

        if curtimestep < len(modeltimes):

            # %% Update state variables (model measurements) with forward model run
            # Transform Y to normal space [m/s] for forward simulation
            print('Making new model predictions time step:%s (python idx)\nRealization:%s' % (curtimestep, NoReal))
            modmeas_new, Y_dummy = hgr.getstates(homedir, ii, str_assim, nnodes, mymode, modeltimes,
                                                              Y_i_upd=np.exp(Y_i),
                                                              curtimestep=curtimestep, mytype=mytype,
                                                              initial_head=ini_head,
                                                              neg2zero=neg2zero, head2dwdn=head2dwdn, cumbtc=cumbtc,
                                                              norm=norm,
                                                              moments=moments, OWnode=OWnode, porosity=porosity,
                                                              storativity=storativity, dispersivity=dispersivity,
                                                              subgrid=subgrid,
                                                              subgridnodes=subgridnodes,
                                                              subgridelem=subgridelem, hgs=hgs)
            del Y_dummy

        else:
            print('Returning last updated parameters...')
            modmeas_new = np.empty(fieldmeas.shape)
            # if mymode == 'tr_':
            #     headfield_i = np.empty((1,))
            # else:
            #     headfield_i = np.empty((nnodes,))

        restart_flag = False
        if restart_flag is False:  # or ac_flag == True would also work
            print('Model measurements of realization %d updated.' % NoReal)

        # Join again kkk field with extra parameters if needed:
        if (porosity or storativity or dispersivity) is True:
            Y_i = np.r_[Y_i, extra_param]

    return Y_i, modmeas_new, res_old, Likelihood_old


def read_iter_data(mydir, myType='', my_timest=''):
    """
    mydir:       Str, name of the project(folder) containing the results of the inversion
    myType:         Str, type of data to load. Param -> 'Y'; ModelOutputs -> 'ModMeas'; CovMatrix -> 'Qmy'
    my_timest:      Str, number of time step to take data from
    what:           Str, shrink, expand or nothing to do with the stored data
    """
    if not os.path.exists(mydir):
        sys.exit('Directory where data is stored has been set incorrectly!')

    if myType == '':
        sys.exit('No type of data defined. Restarting is not possible!')
    lst_temp = os.listdir(mydir)
    lst_temp.sort()
    lst = []

    # First filter of the strings: get those corresponding to the type of data requested (e.g. Model outputs)
    for ss in lst_temp:
        if ss.startswith(myType):
            if my_timest in ss:
                lst.append(ss)
    try:
        lst = lst[-1]
    except (IndexError, ValueError, TypeError, AttributeError):
        print('Wrong cur time definition, no data was loaded!')

    if '.npy' in lst:
        mydata = np.load(os.path.join(mydir, lst))
    if '.txt' in lst:
        mydata = np.loadtxt(os.path.join(mydir, lst))
    if '.dat' in lst:
        mydata = np.loadtxt(os.path.join(mydir, lst))

    return mydata


def checkLastIterFiles(Inv_name, my_timest=''):
    """
    Inv_name:       Str, name of the project(folder) containing the results of the inversion
    my_timest:      Str, number of time step to take data from
    """

    KalmanFilterPath = 'kalmanfilter'
    # %% Define directories:
    mainPath = os.path.dirname(__file__)
    mydataFiles = os.path.abspath(os.path.join(mainPath, '..', KalmanFilterPath, 'Realizations', Inv_name))

    if not os.path.exists(mydataFiles):
        sys.exit('Directory where data is stored has been set incorrectly!')

    lst_temp = os.listdir(mydataFiles)
    lst_temp.sort()
    lst = []
    lst2 = []
    myiter = []
    # get a list of results stored from the last iteration and last time step:
    for myid, ss in enumerate(lst_temp):
        if my_timest in ss:
            lst.append(ss)
            myiter.append(int(ss.split('_')[1].split('iter')[1]))
    if len(lst) > 3:
        for ii in lst:
            if ('%.3d' % (max(myiter))) in ii:
                lst2.append(ii)
        if len(lst2) == 3:
            myflag = 'Done'

    elif len(lst) == 3:
        myflag = 'Done'

    else:
        if len(lst) == 1:
            assert 'ModMeas' in lst[0], 'Unexpected error'
            myflag = 'NotDone'

    return 'Done', ('%.3d' % (max(myiter)))



def update_eq(Y, fieldmeas, modmeas, R, parmatmult, Kgain, dY_max):
    """ Compute the update equation of the Ensemble Kalman Filter
    :param Y:
    :param fieldmeas:
    :param modmeas:
    :param R:
    :param parmatmult:
    :param Kgain:
    :param dY_max:
    :return:
        Updated parameter field (1d matrix)
    """

    # %% Compute residuals and likelihoods:
    res_old, Likelihood_old = res_likelihood(fieldmeas, modmeas, R, parmatmult, mymethod='solve')
    # todo: include the accepting rejection criteria from Nowak
    # Compute  Kalman gain * Residuals ===> Qsy (Qyy + R)-1 [Yo - (Yui + Ei)]
    # Y_term_upd = (Qym.dot(np.linalg.inv(Qmm_new))).dot(res_old)  # (nelements x 1)
    if parmatmult is False:
        Y_term_upd = np.dot(Kgain, res_old)
    elif parmatmult is True:
        Y_term_upd = mm.matmul_locopt(Kgain, res_old, ncpu=1)

    # %% Apply the step control and update it (according to Eq 3 Schoniger 2012):
    # s_i,c (or Y_i,c) = Y_i,u + Y_term_upd
    Y_term_upd = step_control(Y_term_upd, dY_max)  # todo: check this step controller
    if Y.ndim == 1:
        Y_i = Y[:, np.newaxis] + Y_term_upd
    else:
        Y_i = Y + Y_term_upd

    return Y_i

